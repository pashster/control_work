# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
fixtures_path = Rails.root.join('app', 'assets', 'images')

admin = User.create(name: 'Admin',
                    email: "admin@mail.ru",
                    password: "qweqwe", password_confirmation: "qweqwe",
                    admin: true)

10.times do |n|
  User.create(name: Faker::Name.first_name, email: "example_#{n+1}@mail.ru", password: "qweqwe", password_confirmation: "qweqwe", role: "user")
end

15.times do
  Book.create(title: Faker::Book.title, desc: Faker::Lorem.paragraph, user_id: rand(1..10),
              pictures: [File.new(fixtures_path.join("pic_#{rand(1..8)}.jpg")), File.new(fixtures_path.join("pic_#{rand(1..8)}.jpg")), File.new(fixtures_path.join("pic_#{rand(1..8)}.jpg"))])
end

30.times do |i|
  Review.create(body: Faker::Lorem.paragraph, rate: rand(1..5), user_id: rand(1..10), book_id: rand(1..15))
end

5.times do
  Author.create(name: Faker::Book.author, surname: Faker::Name.last_name, desc: Faker::Name.title)
end

7.times do
  Category.create(name: Faker::Book.genre, desc: Faker::Lorem.paragraph, author_id: rand(1..5))
end

15.times do |n|
  GanreFromBook.create(book_id: "#{n+1}", category_id: rand(1..7))
end

15.times do |n|
  AuthorBook.create(book_id: "#{n+1}", author_id: rand(1..5))
end

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
