class CreateGanreFromBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :ganre_from_books do |t|
      t.references :book, foreign_key: true
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
