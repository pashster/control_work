class AuthorsController < ApplicationController
  load_and_authorize_resource
  def new
    @author = Author.new
  end

  def create
    @author = Author.new(author_params)

    respond_to do |format|
      if @author.save
        format.html { redirect_to root_path, notice: 'Author added!' }
        format.json { render :show, status: :created, location: @author }
      else
        format.html { render :new }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def author_params
    params.require(:author).permit(:name, :desc, :surname)
  end
end
