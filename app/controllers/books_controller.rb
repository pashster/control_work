class BooksController < ApplicationController
  load_and_authorize_resource

  def new
    @book = Book.new
    @authors = Author.all
    @categories = Category.all
  end

  def show
    @book = Book.find(params[:id])
    @pictures = @book.pictures
    @reviews = @book.reviews
    @review = Review.new
  end

  def index
    @books = Book.paginate(:page => params[:page], per_page: 6)
    @categories = Category.all
  end

  def create
    @book = Book.new(book_params)

    respond_to do |format|
      if @book.save
        format.html { redirect_to book_path(@book), notice: 'The book is under moderation and will be added after approval by the administrator' }
        format.json { render :show, status: :created, location: @book }
      else
        format.html { render :new }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end

  end

  private

  def book_params
    params.require(:book).permit(:title, :desc, :user_id, {pictures: []}, author_ids: [] )
  end
end
