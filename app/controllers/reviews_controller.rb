class ReviewsController < ApplicationController
  load_and_authorize_resource
  def new
    @review = Review.new
  end

  def create
    @review = Review.new(review_params)
    respond_to do |format|
      if @review.save
        format.html { redirect_to :back, notice: 'Review added!' }
        format.json { render :show, status: :created, location: @review }
      else
        format.html { redirect_to :back, notice: 'Enter all inputs!' }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @review = Review.find(params[:id])
    if @review.destroy
      flash[:success] = "Review deleted!"
      redirect_back(fallback_location: root_path)
    else
      flash[:alert] = "ERROR!"
      redirect_back(fallback_location: root_path)
    end
  end

  private

  def review_params
    params.require(:review).permit(:body, :rate, :user_id, :book_id)
  end
end
