class Book < ApplicationRecord
  has_many :authors, through: :author_books
  has_many :author_books
  belongs_to :user
  has_many :categories, through: :ganre_from_books
  has_many :ganre_from_books
  has_many :reviews

  mount_uploaders :pictures, PictureUploader
  serialize :pictures, JSON

  validates :title, presence: true
  validates :desc, presence: true

  def rate
    @result = 0
    self.reviews.each do |result|
      @result = @result + result.rate
    end
    if @result == 0
      @result = 0
    else
      @result = @result / self.reviews.count
      @result = eval(sprintf("%8.1f",@result))
    end
  end

  def rate_author

  end
end
