class User < ApplicationRecord
  before_save { self.name.capitalize! }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :books
  has_many :reviews

  validates :name, presence: true
end
