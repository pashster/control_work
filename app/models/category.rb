class Category < ApplicationRecord
  belongs_to :author
  has_many :books, through: :ganre_from_books
  has_many :ganre_from_books
end
